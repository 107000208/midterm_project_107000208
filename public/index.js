function initApp() {
    setInterval(function() {
        $(".cube").toggleClass("move");
      }, 1000);
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(){
            window.location.href = "chat.html";
        }).catch(function(error){
            create_alert('error', error.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });

    btnGoogle.addEventListener('click', function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
            window.location.href = "chat.html";
        }).catch(function (error) {
            create_alert('error', error.message);
        });
    });

    btnSignUp.addEventListener('click', function () {        
        var email = txtEmail.value;
        var password = txtPassword.value;
        var alertarea = document.getElementById('custom-alert');
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(e){
            str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + 'success' + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            alertarea.innerHTML = str_html;
            // create_alert('success',"Sign up success!");
            txtEmail.value = "";
            txtPassword.value = "";
        }).catch(function(errror){
            // create_alert('error',e.message);
            str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + 'error' + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            alertarea.innerHTML = str_html;
            txtEmail.value = "";
            txtPassword.value = "";
        });
    });
}

// Custom alert
// function create_alert(type, message) {
//     var alertarea = document.getElementById('custom-alert');
//     if (type == "success") {
//         str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
//         alertarea.innerHTML = str_html;
//     } else if (type == "error") {
//         str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
//         alertarea.innerHTML = str_html;
//     }
// }

window.onload = function () {
    initApp();
};

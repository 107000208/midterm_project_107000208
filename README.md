# Software Studio 2020 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : **Midterm_Project_107000208**
* Key functions (add/delete)
- **In "Sign In & Sign Up" page:**
1. `btnLogin.addEventListener('click', function () {...}`
2. `btnGoogle.addEventListener('click', function () {...}`
3. `btnSignUp.addEventListener('click', function () {...}`


- **In "Chat Room" page:**
1. `function InitMessage() {...}`
2. `function enterChat(buttonId) {...} //when choose a group to add.`

* Other functions (add/delete)
1. Get time: `getMonth()、getDate()、getHours()、getMinutes()`
2. Change background: `$('#demolist li').on('click', function(){...}`
3. Leave Group button: `function leaveChat(buttonId){...}`
4. Add New Friend: `addNewFriendBtn.addEventListener('click', function() {...}`

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|15%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|20%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://chatroom-598bb.web.app](https://)

# Components Description : 
#### Sign In & Sign Up Page
![](https://i.imgur.com/N0Vcg1c.gif)

1. *Use CSS Animation* : 背景會自動做顏色漸層變化
2. *Membership Mechanism / Chrome Notification* : Sign Up 成功或失敗會有chrome notification顯示（如下）
![](https://i.imgur.com/yN28mvX.png =350x70)

![](https://i.imgur.com/DqLObZm.png =350x70)

4. *Third-Party Sign In* : 點擊"Sign in with Google"，可以選擇google帳號登入

#### Chat Room Page

1. Hall:一開始進Chat Room會先看到大廳，每個人都可以在這裡發言（不需權限即可）
2. New Group Button : 點擊可以新增chat room，並跳出Success的chrome nofitication視窗，關閉提醒視窗後，才會更新左邊的group介面
![](https://i.imgur.com/q5u9PQ9.gif)

3. Tool Bar : How / Log out
![](https://i.imgur.com/TVkk25L.png =40%x)

- How: click it to show how to use this chatroom 

![](https://i.imgur.com/zEFvofi.png =75%x)

- Log out :點擊後，會導向回Sing In & Sign Up Page
```
/* chat.js */
var logout = document.getElementById("logoutBtn") ;

logout.addEventListener('click', function(){
firebase.auth().signOut().then(function(){
window.location.href="index.html";
}).catch(e => alert(e.messege));
});
```
# Other Functions Description : 
1. Get Time: 每則訊息皆會即時顯示送出時間
```
/* chat.js */
var date = new Date();
var time = (date.getMonth()+1) + '/' + (date.getDate()) + ' ' + (date.getHours()) + ':' + (date.getMinutes());```

透過date取得時間後，再放進data裡，並push到chatroom中：
var data = {
data: message.value,
name: nickname,
time: time //add msg time
};
chatroom.push(data);
3. *Change Background color* : Blue / White / Pink（如下）
![](https://i.imgur.com/rqjJsEd.gif)
```
/* chat.js */
預設好三種不同顏色，根據user點擊不同的顏色，去做if-else判斷.
$('#demolist li').on('click', function(){
console.log($(this).text());
console.log(typeof($(this).text()));

if($(this).text()=='Blue'){
document.getElementsByTagName("body")[0].style="background-color:#ccf5ff;";
}
else if($(this).text()=='White'){
document.getElementsByTagName("body")[0].style="background-color:#fff3e6;";
}
else if($(this).text()=='Pink'){
document.getElementsByTagName("body")[0].style="background-color:#ffe6e6;";
}
});
```
3. *Leave Group Button* : 當點擊時，會返回大廳
```
/* chat.js */
當點擊時，利用onclick="leaveChat(this.id)"，處理登出和畫面跳轉.
var group_history_html=`<div class="media-body" ><img src="images/business.png"><h5 class="media-heading">${child.key}</h5><button class="btn btn-success" id="button_${child.key}" onclick="enterChat(this.id)">Enter Group</button><button class="btn btn-danger" id="buttonleave_${child.key}" onclick="leaveChat(this.id)">Leave Group</button></div>`;
```
```
/* chat.js */
function leaveChat(buttonId){
//console.log('leave');
window.location.href = 'chat.html';//畫面跳轉.
}
```
4. *Add New Friend* : 透過email添加聊天室好友
![](https://i.imgur.com/ZT8lKJw.gif)

## Security Report (Optional)
1.需要被加入聊天室（以email加入）才有權限進入查看該聊天室訊息，若沒有權限，則會跳出alert視窗提醒，並導向回到大廳（如下）。
![](https://i.imgur.com/Ft0mTM2.gif)


